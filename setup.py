#!/usr/bin/env python

from setuptools import setup, find_packages

setup( name='pyfs',
       version='1.0.0',
       packages=find_packages(),
       install_requires=[
           'oneup',
           'pyyaml',
        ],
        dependency_links=[
            'https://gitlab.com/craigyk/oneup/-/archive/master/oneup-master.tar.gz#egg=oneup',
        ]   
)
